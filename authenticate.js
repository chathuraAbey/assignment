/**
 * Holds API endpoints reponsible for signing up, authenticating a user.
 */
const express = require('express');
const config = require('config');
const router = express.Router();
const User = require('./src/models/user');
const Blacklist = require('./src/models/blacklist');
// Will be using JWT to create, sign, and verify tokens
const jwt = require('jsonwebtoken');
const validator = require('./src/validators/authenticate');
const { handleValidationError } = require('./src/lib/helper');

/* Middleware to handle internal server error */
function handleInternalServerError(err, res) {
    console.error('Internal Server Error: ', err);
    return res.status(500).send({ message: 'Internal Server Error' });
};

/**
 * Authenticate user using username and password. Returns signed accessToken and data if authentication is successful.
 */
router.post('/authenticate/login', validator.authenticate(), handleValidationError, async (req, res) => {

    try {
        // Get the user record based on given email
        let user = await User.findOne({ email: req.body.email }).exec();

        if (!user) {
            return res.status(400).send({ success: false, message: 'Authentication failed. User not found.' });
        } else {
            // Check if password matches
            let isMatch = await user.checkPassword(req.body.password);
            if (!isMatch) {
                return res.status(400).send({ success: false, message: 'Authentication failed. Invalid password.' });
            } else {
                // Prepare token payload
                const payload = {
                    id: user._id,
                    userType: user.userType
                };
                // Sign JWT token payload
                const token = jwt.sign(payload, config.get('authentication.secret'), {
                    expiresIn: config.get('authentication.expiresIn')
                });

                // Return the token and user data
                res.send(201, {
                    success: true,
                    message: 'Successfully authenticated.',
                    'user': {
                        id: user._id,
                        firstName: user.firstName,
                        lastName: user.lastName,
                        email: user.email
                    },
                    accessToken: token
                });
            }
        }
    } catch (err) {
        console.error('Error while authenticating user: ', req.body.email);
        handleInternalServerError(err, res);
    }
});


/**
 * Sign up user to the system.
 */
router.post('/authenticate/signup', validator.signup(), handleValidationError, (req, res) => {
    User.create({
        email: req.body.email,
        password: req.body.password,
        firstName: req.body.firstName,
        lastName: req.body.lastName
    }).then(user => {
        req.session.user = user.dataValues;
        return res.status(201).send(user.toClient());
    }).catch(error => {
        console.error('Error signing up user: ', req.body.email);
        handleInternalServerError(err, res);
    });
});

/**
 * Route middleware to verify a jwt signed token. Checks if the token is valid and not expired.
 */
router.use(function (req, res, next) {

    // Check the token in either body or in header
    let token = req.body.token || req.headers['authorization'];

    // If token exists decode it
    if (token) {
        token = token.replace('Bearer', '');
        token = token.trim();

        Blacklist.findOne({ token: token }).exec().then(blacklist => {
            if (blacklist) {
                // Token is blacklisted. (Logged out)
                return res.json({ success: false, message: 'Failed to authenticate token.' });
            }
            // Verify the token secret and checks it is not expired
            jwt.verify(token, config.get('authentication.secret'), function (err, decoded) {
                if (err) {
                    return res.json({ success: false, message: 'Failed to authenticate token.' });
                } else {
                    // Valid token authenticate user, save decoded information and route to next middleware
                    req.user = decoded;
                    next();
                }
            });
        }).catch(err => {
            console.error('Error accessing token blacklist');
            return handleInternalServerError(err, res);
        });
    } else {
        // No token provided
        return res.status(403).send({
            success: false,
            message: 'No token provided.'
        });
    }
});

/**
 * Logout user by blacklisting JWT token
 */
router.delete('/authenticate/logout', (req, res) => {

    // Check the token in either body or in header
    let token = req.body.token || req.headers['authorization'];
    token = token.replace('Bearer', '');
    token = token.trim();

    Blacklist.create({
        token: token
    }).then(() => {
        req.session.destroy();
        return res.status(200).send();
    }).catch(error => {
        console.error('Error logging out user');
        handleInternalServerError(error, res);
    });
});

/** 
 * Add POST endpoints that only admin should be able to access
 */
router.post(['/products'], (req, res, next) => {

    // Alow type 1 user (admin)
    if (req.user.userType === 1) {
        return next();
    } else {
        return res.status(403).send({ success: false, message: 'Permission denied.' });
    }
});

/** 
 * Add DELETE endpoints that only admin should be able to access
 */
router.delete(['/products/:id', '/users/:id'], (req, res, next) => {

    // Alow type 1 user (admin)
    if (req.user.userType === 1) {
        return next();
    } else {
        return res.status(403).send({ success: false, message: 'Permission denied.' });
    }
});

module.exports = router;