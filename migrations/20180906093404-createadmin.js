'use strict';

var dbm;
var type;
var seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function (options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function (db, callback) {
  db.insert('users', {
    "firstName": "Super",
    "lastName": "Administrator",
    "userType": 1,
    "email": "super@aswat.com",
    "password": "$2b$10$tO7A7AbfGlYIg3Felqx2aO7kyP7uy8qNgfJ8PqS9BL3yJtJtHKRIC"
  }, function (err, res) {
    if (!err) {
      console.log(res.insertedCount + " user(s) created");
    }
    return callback(err, res);
  });
};

exports.down = function (db, callback) {
  async.series([
    db.dropTable.bind(db, 'users')
  ], callback);
};

exports._meta = {
  "version": 1
};
