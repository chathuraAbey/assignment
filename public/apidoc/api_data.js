define({ "api": [
  {
    "type": "post",
    "url": "http://<base-url>/authenticate/login",
    "title": "Login user",
    "description": "<p>Login using username,password and obtain accessToken to be used for API calls.</p>",
    "name": "LoginUser",
    "group": "Authenticate",
    "examples": [
      {
        "title": "Example Request:",
        "content": "{\n  \"email\":\"mymail@gmail.com\",\n  \"password\":\"TESTtest1\"\n}",
        "type": "json"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "firstname",
            "description": "<p>Firstname of the User.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "lastname",
            "description": "<p>Lastname of the User.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>Email of the User.</p>"
          },
          {
            "group": "Success 200",
            "type": "boolean",
            "optional": false,
            "field": "success",
            "description": "<p>True if login is success; false otherwise.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Login message dependends on login status.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "accessToken",
            "description": "<p>Access token to be used by the user to authenticate API calls.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>User record ObjectID (UUID).</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response Sample:",
          "content": "{\n  \"success\": true,\n  \"message\": \"Successfully authenticated.\",\n  \"user\": {\n      \"id\": \"5b90c167c870c9079836b01c\",\n      \"firstName\": \"layansan\",\n      \"lastName\": \"rajendram\",\n      \"email\": \"mymail@gmail.com\"\n  },\n  \"accessToken\": \"XXXXXGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjViOGNlZDM0ZmM5NWI3MmFhYzJjZWZhOSIsInVzZXJUeXBlIjoyLCJpYXQiOjE1MzYyMTM4NTAsImV4cCI6MTUzNjMwMDI1MH0.VXlgNZJe9zjhwawgwfE2b2soMluKV93v4Hx4V1SulGE\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "data/apidoc/authenticate.js",
    "groupTitle": "Authenticate"
  },
  {
    "type": "delete",
    "url": "http://<base-url>/authenticate/logout",
    "title": "Logout user",
    "description": "<p>Logout user.</p>",
    "name": "LogoutUser",
    "group": "Authenticate",
    "version": "0.0.0",
    "filename": "data/apidoc/authenticate.js",
    "groupTitle": "Authenticate"
  },
  {
    "type": "post",
    "url": "http://<base-url>/cart",
    "title": "Add to Cart",
    "description": "<p>Add a product to user's shopping cart.</p> <ul> <li>Authorization token required in header.</li> </ul>",
    "header": {
      "examples": [
        {
          "title": "Authorization header-Example:",
          "content": "{\n     \"Authorization\": \"Bearer XXXXXGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjViOGNlZDM0ZmM5NWI3MmFhYzJjZWZhOSIsInVzZXJUeXBlIjoxLCJpYXQiOjE1MzYwNDQzNTIsImV4cCI6MTUzNjEzMDc1Mn0.l3WHJjBWAROo-3nvlbwQQ82lUDsZVsrk-zbnKYekt0Y\"\n}",
          "type": "json"
        }
      ]
    },
    "name": "AddToCart",
    "group": "Cart",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "productId",
            "description": "<p>Product ObjectID (UUID).</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "quantity",
            "description": "<p>Purchase quantity.</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example Request:",
        "content": "{\n     \"productId\": \"5b8d17f050bc7309c0019d8c\",\n     \"quantity\": 1\n }",
        "type": "json"
      }
    ],
    "success": {
      "examples": [
        {
          "title": "Success-Response Sample:",
          "content": "{\n \"items\": [\n      {\n           \"id\": \"5b8d17f050bc7309c0019d8c\",\n           \"name\": \"myproduct 1\",\n           \"description\": \"\",\n           \"price\": 100,\n           \"quantity\": 1\n       }\n   ],\n   \"totals\": 100\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "data/apidoc/cart.js",
    "groupTitle": "Cart"
  },
  {
    "type": "patch",
    "url": "http://<base-url>/cart",
    "title": "Update cart",
    "description": "<p>Update user's shopping cart. Provide productId, quantity as single value or as an array to do multiple update</p> <ul> <li>Authorization token required in header.</li> </ul>",
    "header": {
      "examples": [
        {
          "title": "Authorization header-Example:",
          "content": "{\n     \"Authorization\": \"Bearer XXXXXGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjViOGNlZDM0ZmM5NWI3MmFhYzJjZWZhOSIsInVzZXJUeXBlIjoxLCJpYXQiOjE1MzYwNDQzNTIsImV4cCI6MTUzNjEzMDc1Mn0.l3WHJjBWAROo-3nvlbwQQ82lUDsZVsrk-zbnKYekt0Y\"\n}",
          "type": "json"
        }
      ]
    },
    "name": "AddToCart",
    "group": "Cart",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "productId",
            "description": "<p>Product ObjectID (UUID) []Array for multiple.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "quantity",
            "description": "<p>Purchase quantity []Array for multiple..</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example Request:",
        "content": "{\n     \"productId\": [\"5b8d17f050bc7309c0019d8c\", \"5b8d17f050bc7309c0019d8d\"],\n     \"quantity\": [1, 2]\n }",
        "type": "json"
      }
    ],
    "success": {
      "examples": [
        {
          "title": "Success-Response Sample:",
          "content": "{\n \"items\": [\n      {\n           \"id\": \"5b8d17f050bc7309c0019d8c\",\n           \"name\": \"myproduct 1\",\n           \"description\": \"\",\n           \"price\": 100,\n           \"quantity\": 1\n       }\n   ],\n   \"totals\": 100\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "data/apidoc/cart.js",
    "groupTitle": "Cart"
  },
  {
    "type": "delete",
    "url": "http://<base-url>/cart",
    "title": "Empty Cart",
    "description": "<p>Empty user's shopping cart.</p> <ul> <li>Authorization token required in header.</li> </ul>",
    "header": {
      "examples": [
        {
          "title": "Authorization header-Example:",
          "content": "{\n     \"Authorization\": \"Bearer XXXXXGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjViOGNlZDM0ZmM5NWI3MmFhYzJjZWZhOSIsInVzZXJUeXBlIjoxLCJpYXQiOjE1MzYwNDQzNTIsImV4cCI6MTUzNjEzMDc1Mn0.l3WHJjBWAROo-3nvlbwQQ82lUDsZVsrk-zbnKYekt0Y\"\n}",
          "type": "json"
        }
      ]
    },
    "name": "EmptyCart",
    "group": "Cart",
    "version": "0.0.0",
    "filename": "data/apidoc/cart.js",
    "groupTitle": "Cart"
  },
  {
    "type": "get",
    "url": "http://<base-url>/cart",
    "title": "Get Cart",
    "description": "<p>Retrieve shopping cart of the logged in user who makes the request</p>",
    "name": "GetCart",
    "group": "Cart",
    "examples": [
      {
        "title": "Example Request:",
        "content": "{\n  \"firstName\":\"Layansan\",\n  \"lastName\":\"Rajendram\",\n  \"email\":\"mymail@gmail.com\",\n  \"password\":\"TESTtest1\"\n}",
        "type": "json"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Item name.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "description",
            "description": "<p>Item description.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "Price",
            "description": "<p>Item price (of one unit).</p>"
          },
          {
            "group": "Success 200",
            "type": "id",
            "optional": false,
            "field": "id",
            "description": "<p>Product record ObjectID (UUID).</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "totals",
            "description": "<p>Total cart value.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response Sample:",
          "content": "{\n \"items\": [\n      {\n           \"id\": \"5b8d17f050bc7309c0019d8c\",\n           \"name\": \"myproduct 1\",\n           \"description\": \"\",\n           \"price\": 100,\n           \"quantity\": 1\n       }\n   ],\n   \"totals\": 100\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "data/apidoc/cart.js",
    "groupTitle": "Cart"
  },
  {
    "type": "post",
    "url": "http://<base-url>/cart/product/:productId",
    "title": "Remove from Cart",
    "description": "<p>Remove a product from user's shopping cart.</p> <ul> <li>Authorization token required in header.</li> </ul>",
    "header": {
      "examples": [
        {
          "title": "Authorization header-Example:",
          "content": "{\n     \"Authorization\": \"Bearer XXXXXGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjViOGNlZDM0ZmM5NWI3MmFhYzJjZWZhOSIsInVzZXJUeXBlIjoxLCJpYXQiOjE1MzYwNDQzNTIsImV4cCI6MTUzNjEzMDc1Mn0.l3WHJjBWAROo-3nvlbwQQ82lUDsZVsrk-zbnKYekt0Y\"\n}",
          "type": "json"
        }
      ]
    },
    "name": "RemoveFromCart",
    "group": "Cart",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "productId",
            "description": "<p>Product ObjectID (UUID).</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response Sample:",
          "content": "{\n \"items\": [\n      {\n           \"id\": \"5b8d17f050bc7309c0019d8c\",\n           \"name\": \"myproduct 1\",\n           \"description\": \"\",\n           \"price\": 100,\n           \"quantity\": 1\n       }\n   ],\n   \"totals\": 100\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "data/apidoc/cart.js",
    "groupTitle": "Cart"
  },
  {
    "type": "patch",
    "url": "http://<base-url>/users/:id",
    "title": "Update User",
    "description": "<p>Update existing user details such as firstName, lastName. This endpoint can not be used to update login information such as email/password</p> <ul> <li>Authorization token required in header.</li> </ul>",
    "header": {
      "examples": [
        {
          "title": "Authorization header-Example:",
          "content": "{\n     \"Authorization\": \"Bearer XXXXXGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjViOGNlZDM0ZmM5NWI3MmFhYzJjZWZhOSIsInVzZXJUeXBlIjoxLCJpYXQiOjE1MzYwNDQzNTIsImV4cCI6MTUzNjEzMDc1Mn0.l3WHJjBWAROo-3nvlbwQQ82lUDsZVsrk-zbnKYekt0Y\"\n}",
          "type": "json"
        }
      ]
    },
    "name": "GetUser",
    "group": "User",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "id",
            "description": "<p>User ObjectID (UUID).</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example Request Body:",
        "content": "{\n  \"firstName\":\"Layansan\",\n  \"lastName\":\"Rajendram\"\n}",
        "type": "json"
      },
      {
        "title": "Example Request Path:",
        "content": "/users/5b90c167c870c9079836b01c",
        "type": "json"
      }
    ],
    "version": "0.0.0",
    "filename": "data/apidoc/user.js",
    "groupTitle": "User"
  },
  {
    "type": "get",
    "url": "http://<base-url>/users/:id",
    "title": "Get User",
    "description": "<p>Retrieve existing user details.</p> <ul> <li>Authorization token required in header.</li> </ul>",
    "header": {
      "examples": [
        {
          "title": "Authorization header-Example:",
          "content": "{\n     \"Authorization\": \"Bearer XXXXXGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjViOGNlZDM0ZmM5NWI3MmFhYzJjZWZhOSIsInVzZXJUeXBlIjoxLCJpYXQiOjE1MzYwNDQzNTIsImV4cCI6MTUzNjEzMDc1Mn0.l3WHJjBWAROo-3nvlbwQQ82lUDsZVsrk-zbnKYekt0Y\"\n}",
          "type": "json"
        }
      ]
    },
    "name": "GetUser",
    "group": "User",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "id",
            "description": "<p>User ObjectID (UUID).</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response Sample:",
          "content": "{\n     \"lastName\": \"rajendram\",\n     \"userType\": 2,\n     \"email\": \"mymail@gmail.com\",\n     \"firstName\": \"layansan\",\n     \"createdAt\": \"2018-09-06T05:55:51.208Z\",\n     \"updatedAt\": \"2018-09-06T05:55:51.208Z\",\n     \"id\": \"5b90c167c870c9079836b01c\"\n }",
          "type": "json"
        }
      ]
    },
    "examples": [
      {
        "title": "Example Request:",
        "content": "/users/5b90c167c870c9079836b01c",
        "type": "json"
      }
    ],
    "version": "0.0.0",
    "filename": "data/apidoc/user.js",
    "groupTitle": "User"
  },
  {
    "type": "get",
    "url": "http://<base-url>/users",
    "title": "List users",
    "description": "<p>Retrieve list of Users.</p> <ul> <li>Authorization token required in header.</li> </ul>",
    "header": {
      "examples": [
        {
          "title": "Authorization header-Example:",
          "content": "{\n     \"Authorization\": \"Bearer XXXXXXciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjViOGNlZDM0ZmM5NWI3MmFhYzJjZWZhOSIsInVzZXJUeXBlIjoxLCJpYXQiOjE1MzYwNDQzNTIsImV4cCI6MTUzNjEzMDc1Mn0.l3WHJjBWAROo-3nvlbwQQ82lUDsZVsrk-zbnKYekt0Y\"\n}",
          "type": "json"
        }
      ]
    },
    "name": "ListUsers",
    "group": "User",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "number",
            "optional": true,
            "field": "page",
            "description": "<p>Page number.</p>"
          },
          {
            "group": "Parameter",
            "type": "number",
            "optional": true,
            "field": "limit",
            "description": "<p>Records per page.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n  \"docs\": [\n      {\n          \"_id\": \"5b8ced34fc95b72aac2cefa9\",\n          \"lastName\": \"rajendram\",\n          \"userType\": 2,\n          \"email\": \"layansann@example.com\",\n          \"firstName\": \"layansan\",\n          \"createdAt\": \"2018-09-03T08:13:40.698Z\",\n          \"updatedAt\": \"2018-09-03T08:13:40.698Z\",\n          \"id\": \"5b8ced34fc95b72aac2cefa9\"\n      },\n      {\n          \"_id\": \"5b90c167c870c9079836b01c\",\n          \"lastName\": \"rajendram\",\n          \"userType\": 2,\n          \"email\": \"layansan+n@example.test\",\n          \"firstName\": \"layansan\",\n          \"createdAt\": \"2018-09-06T05:55:51.208Z\",\n          \"updatedAt\": \"2018-09-06T05:55:51.208Z\",\n          \"id\": \"5b90c167c870c9079836b01c\"\n      }\n  ],\n  \"total\": 2,\n  \"limit\": 3,\n  \"page\": 1,\n  \"pages\": 1\n}",
          "type": "json"
        }
      ]
    },
    "examples": [
      {
        "title": "Example Request:",
        "content": "/users?page=1&limit=3",
        "type": "json"
      }
    ],
    "version": "0.0.0",
    "filename": "data/apidoc/user.js",
    "groupTitle": "User"
  },
  {
    "type": "post",
    "url": "http://<base-url>/authenticate/signup",
    "title": "User Signup",
    "description": "<p>Sign up a new user (customer) to the system.</p>",
    "name": "UserSignup",
    "group": "User",
    "examples": [
      {
        "title": "Example Request:",
        "content": "{\n  \"firstName\":\"Layansan\",\n  \"lastName\":\"Rajendram\",\n  \"email\":\"mymail@gmail.com\",\n  \"password\":\"TESTtest1\"\n}",
        "type": "json"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "firstname",
            "description": "<p>Firstname of the User.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "lastname",
            "description": "<p>Lastname of the User.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>Email of the User.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "userType",
            "description": "<p>Type of the User 1:Staff, 2:Customer.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "createdAt",
            "description": "<p>User record created timestamp.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "updatedAt",
            "description": "<p>User record updated timestamp.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>User record ObjectID (UUID).</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response Sample:",
          "content": "{\n     \"lastName\": \"rajendram\",\n     \"userType\": 2,\n     \"email\": \"mymail@gmail.com\",\n     \"firstName\": \"layansan\",\n     \"createdAt\": \"2018-09-06T05:55:51.208Z\",\n     \"updatedAt\": \"2018-09-06T05:55:51.208Z\",\n     \"id\": \"5b90c167c870c9079836b01c\"\n }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "data/apidoc/authenticate.js",
    "groupTitle": "User"
  }
] });
