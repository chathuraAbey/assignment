define({
  "name": "ASWAT REST API BY LAYANSAN",
  "version": "0.0.1",
  "description": "This document describes the ASWAT e-commerce REST API functions.",
  "template": {
    "withCompare": false
  },
  "sampleUrl": false,
  "defaultVersion": "0.0.0",
  "apidoc": "0.3.0",
  "generator": {
    "name": "apidoc",
    "time": "2018-09-06T08:47:49.158Z",
    "url": "http://apidocjs.com",
    "version": "0.17.6"
  }
});
