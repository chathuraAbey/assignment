const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');

const ProductSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, 'MISSING_MANDATORY_ATTRIBUTE'],
        maxlength: [200, 'EXCEED_CHARACTER_LENGTH'],
        trim: true
    },
    description: {
        type: String,
        maxlength: [1000, 'EXCEED_CHARACTER_LENGTH'],
        trim: true,
        default: ''
    },
    price: {
        type: Number,
        min: 0,
        default: 0,
        required: [true, 'MISSING_MANDATORY_ATTRIBUTE']
    },
    quantity: {
        type: Number,
        min: 0,
        default: 1
    }
}, {
        usePushEach: true,
        timestamps: true,
        collation: {
            locale: 'en_US',
            strength: 1
        }
    }
);

ProductSchema.method('toClient', function () {
    // Rename fiels, remove unwanted fields
    var obj = this.toObject();
    obj.id = obj._id;
    delete obj._id;
    delete obj.__v;
    return obj;
});

ProductSchema.plugin(mongoosePaginate);

module.exports = mongoose.model('Product', ProductSchema);