const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');
const emailValidator = require("email-validator");
const bcrypt = require('bcrypt'),
    SALT_WORK_FACTOR = 10;

// Validate email.
function email(value) {
    return emailValidator.validate(value);
}

const UserSchema = new mongoose.Schema({
    firstName: {
        type: String,
        required: [true, 'MISSING_MANDATORY_ATTRIBUTE'],
        maxlength: [60, 'EXCEED_CHARACTER_LENGTH'],
        trim: true
    },
    lastName: {
        type: String,
        maxlength: [60, 'EXCEED_CHARACTER_LENGTH'],
        trim: true,
        default: ''
    },
    email: {
        type: String,
        unique: true,
        maxlength: [254, 'EXCEED_CHARACTER_LENGTH'],
        validate: [email, 'INVALID_EMAIL'],
        sparse: true
    },
    password: {
        type: String,
        default: null,
        trim: true
    },
    userType: {
        // 1: Admin, 2: Customer
        type: Number,
        min: 1,
        max: 2,
        default: 2,
        required: [true, 'MISSING_MANDATORY_ATTRIBUTE'],
    },
}, {
        usePushEach: true,
        timestamps: true,
        collation: {
            locale: 'en_US',
            strength: 1
        }
    }
);

UserSchema.pre('save', function (next) {
    let user = this;

    // Has only if the password is new or modified
    if (!user.isModified('password')) return next();

    // Generate salt
    bcrypt.genSalt(SALT_WORK_FACTOR, function (err, salt) {
        if (err) return next(err);

        // Hash the password using new salt
        bcrypt.hash(user.password, salt, (err, hash) => {
            if (err) return next(err);

            // override the cleartext password with the hashed one
            user.password = hash;
            next();
        });
    });
});

UserSchema.method('toClient', function () {
    // Rename fiels, remove unwanted fields
    var obj = this.toObject();
    obj.id = obj._id;
    delete obj._id;
    delete obj.__v;
    // Should always do this when we send the resposne to client
    delete obj.password;
    return obj;
});

UserSchema.method('checkPassword', function (candidate) {
    let password = this.password;
    return new Promise(function (resolve, reject) {
        bcrypt.compare(candidate, password, (err, isMatch) => {
            if (err) {
                reject(err);
            }
            resolve(isMatch);
        });
    });
});

UserSchema.plugin(mongoosePaginate);

module.exports = mongoose.model('User', UserSchema);