const mongoose = require('mongoose');
const config = require('config');

const BlacklistSchema = new mongoose.Schema({
    token: {
        type: String,
        required: true
    },
    expire_at: { type: Date, default: Date.now, expires: config.get('session.maxAge') }
});
module.exports = mongoose.model('Blacklist', BlacklistSchema);