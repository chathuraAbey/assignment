(() => {

    'use strict';

    const Products = require('../models/product');
    const Cart = require('../lib/cart');

    /* Middleware function to handle internal error */
    function handleError(err, res) {
        console.error('Internal Server Error: ', err);
        return res.status(500).send(err);
    }

    /**
     * Initialize cart in session if it wasn't
     */
    exports.initializeSession = function (req, res, next) {
        if (!req.session.cart) {
            req.session.cart = {
                items: [],
                totals: 0.00
            };
        }
        return next();
    };

    /**
     * Get User's shopping cart from the session
     */
    exports.get = function (req, res) {
        return res.status(200).send(req.session.cart);
    };

    /**
     * Remove an item from shopping cart
     */
    exports.removeFromCart = function (req, res) {
        Cart.removeFromCart(req.body.productId, req.session.cart);
        return res.status(200).send(req.session.cart);
    };

    /**
     * Empty user's shopping cart
     */
    exports.emptyCart = function (req, res) {
        Cart.emptyCart(req);
        return res.status(200).send();
    };

    /**
     * Add an item to user's shopping cart
     */
    exports.addToCart = function (req, res) {
        let qty = parseInt(req.body.quantity, 10);
        let productId = req.body.productId;
        if (qty > 0) {
            Products.findOne({ _id: productId }).then(produdct => {

                // Check the stock availability
                // Reducing the product stock should be done when user checkout
                if (produdct.quantity < qty) {
                    return res.status(400).send({ message: 'INSUFFICIENT_STOCK' });
                }
                let cart = req.session.cart;
                Cart.addToCart(produdct, qty, cart);
                return res.status(200).send(req.session.cart);
            }).catch(err => {
                return handleError(err, res);
            });
        } else {
            return res.status(200).send(req.session.cart);
        }
    };

    /**
     * Update shopping cart. Expects productId and quantity could be single value or array.
     */
    exports.updateCart = function (req, res) {
        let ids = req.body["productId"];
        let qtys = req.body["quantity"];
        let cart = (req.session.cart) ? req.session.cart : null;
        let idArr = (!Array.isArray(ids)) ? [ids] : ids;
        let quantityArr = (!Array.isArray(qtys)) ? [qtys] : qtys;
        Cart.updateCart(idArr, quantityArr, cart);
        return res.status(200).send(req.session.cart);
    };
})();