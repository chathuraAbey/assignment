(() => {

    'use strict';
    const Product = require('../models/product');

    /* Middleware function to handle internal error */
    function handleError(err, res) {
        console.error('Internal Server Error: ', err);
        return res.status(500).send(err);
    }

    /**
     * Create new Product
     */
    exports.create = function (req, res) {

        let product = new Product({
            name: req.body.name,
            description: req.body.description,
            price: req.body.price,
            quantity: req.body.quantity
        });

        // Save the Product
        product.save()
            .then(() => {
                return res.status(201).json(product);
            }).catch(err => {
                return handleError(err, res);
            })
    };

    /**
     * Get single Product
     */
    exports.get = function (req, res) {
        Product.findOne({ _id: req.params.id }).exec()
            .then((product) => {
                res.json(product.toClient());
            }).catch(err => {
                return handleError(err, res);
            })
    };

    /** 
     * List Products
     */
    exports.list = function (req, res) {
        let page = (req.query.page) ? parseInt(req.query.page) : 1;
        let limit = (req.query.limit) ? parseInt(req.query.limit) : 10;
        Product.paginate({}, { page: page, limit: limit, lean: true, leanWithId: true })
            .then(response => {
                res.json(response);
            })
            .catch(err => {
                handleError(err, res);
            });
    };

    /**
     * Delete a Product
     */
    exports.delete = function (req, res) {
        Product.deleteOne({ _id: req.params.id }).exec()
            .then(() => {
                res.status(200).send();
            }).catch(err => {
                return handleError(err, res);
            });
    };

})();