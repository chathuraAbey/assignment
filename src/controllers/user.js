(() => {

    'use strict';
    const User = require('../models/user');
    const responseMessages = require('../lib/responseMessages');


    /* Middleware function to handle internal error */
    function handleError(err, res) {
        console.error('Internal Server Error: ', err);
        return res.status(500).send(err);
    }

    /**
     * POST: Sign up new User
     */
    exports.create = function (req, res) {

        // Prepare user object
        let user = new User({
            firstName: req.body.firstName,
            lastName: req.body.lastName,
            email: req.body.email,
            password: req.body.password
        });

        // Save the user
        user.save(function (err) {
            if (err) handleError(err, res);

            console.log('New user sign up: ', req.body.email);
            res.json({ success: true });
        });
    };

    /**
     * GET: Single User
     */
    exports.get = function (req, res) {
        User.findOne({ _id: req.params.id }, function (err, user) {
            if (err) handleError(err, res);
            if (user) {
                return res.json(user.toClient());
            } else {
                return res.status(400).json({ message: responseMessages.RECORD_NOT_FOUND });
            }
        });
    };

    /**
     * GET: List Users
     */
    exports.list = function (req, res) {
        let page = (req.query.page) ? parseInt(req.query.page) : 1;
        let limit = (req.query.limit) ? parseInt(req.query.limit) : 10;
        User.paginate({}, { page: page, limit: limit, select: 'id firstName lastName email userType createdAt updatedAt', lean: true, leanWithId: true })
            .then(response => {
                res.json(response);
            })
            .catch(err => {
                handleError(err, res);
            });
    };

    /** 
     * PATCH Update User.
     * This doesn't update the password or email for security reason forgot passowrd/change email API should be used for that reason.
     */
    exports.update = function (req, res) {

        delete req.body.email;
        delete req.body.password;
        delete req.body.userType;
        delete req.body.createdAt;
        delete req.body.updatedAt;

        // Update user record
        User.update({ _id: req.params.id }, req.body).exec().then(user => {
            if (user) {
                return res.status(200).send();
            } else {
                return res.status(400).json({ message: responseMessages.RECORD_NOT_FOUND });
            }
        }).catch(err => {
            console.log('Error updating user');
            handleError(err, res)
        });
    }

    /**
     * Delete a User
     */
    exports.delete = function (req, res) {
        User.deleteOne({ _id: req.params.id }).exec()
            .then(() => {
                res.status(200).send();
            }).catch(err => {
                return handleError(err, res);
            });
    };
})();