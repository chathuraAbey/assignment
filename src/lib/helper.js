(() => {

    'use strict';

    const { validationResult } = require('express-validator/check');
    const responseMessages = require('../lib/responseMessages');

    /**
     * Middleware function to check validation errors.
     * If there is an error return HTTP error 400 (Bad Request)
     * @param {Object} req - Request object
     * @param {Object} res - Response object
     * @param {Object} next - Next route
     */
    function handleValidationError(req, res, next) {

        const errors = validationResult(req);

        if (!errors.isEmpty()) {
            let errorData = errors.array({ onlyFirstError: true }).shift();
            let param = errorData['param'];
            if (errorData.nestedErrors) {
                param = '';
                for (let i = 0; i < errorData.nestedErrors.length; i++) {
                    param += i == 0 ? errorData.nestedErrors[i].param : '/' + errorData.nestedErrors[i].param;
                }
            }
            let response = responseMessages.common(errorData['msg'], param);
            return res.status(400).json(response);
        } else {
            next();
        }
    }

    /**
     * Export module fumctions to be accessed from outside
     */
    module.exports = {
        handleValidationError: handleValidationError
    }

})();