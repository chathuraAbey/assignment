'use strict';

/**
 * Class holds Cart static functions
 */
class Cart {

    /**
     * Add given product to shopping cart.
     * 
     * @param {Product} product User adding to cart 
     * @param {Int} quantity Purchase quantity
     * @param {Object} cart User's shopping cart 
     */
    static addToCart(product = null, quantity = 1, cart) {
        if (product) {
            if (!this.inCart(product.id, cart)) {
                let item = {
                    id: product.id,
                    name: product.name,
                    description: product.description,
                    price: product.price,
                    quantity: parseInt(quantity, 10)
                };
                cart.items.push(item);
            }
            this.calculateTotals(cart);
        }
    }

    /**
     * 
     * @param {[ObjectId]} idArr Product Object IDs as an array
     * @param {[Int]} qtyArr Quantities as an array
     * @param {Object} cart User's shopping cart
     */
    static updateCart(idArr = [], qtyArr = [], cart) {
        let map = [];
        let updated = false;

        // Create map with the ids and quantities
        idArr.forEach(id => {
            qtyArr.forEach(quantity => {
                map.push({
                    id: id,
                    quantity: parseInt(quantity, 10)
                });
            });
        });
        map.forEach(obj => {
            cart.items.forEach(item => {
                if (item.id === obj.id) {
                    if (obj.quantity > 0 && obj.quantity !== item.quantity) {
                        // Only update if the quantity has changed
                        item.quantity = obj.quantity;
                        updated = true;
                    }
                }
            });
        });
        if (updated) {
            // Update the totals
            this.calculateTotals(cart);
        }
    }

    /**
     * Removes given product from shopping cart.
     * 
     * @param {ObjectId} id Product ObjectId
     * @param {Object} cart User's shopping cart
     */
    static removeFromCart(id = null, cart) {
        for (let i = 0; i < cart.items.length; i++) {
            let item = cart.items[i];
            if (item.id === id) {
                cart.items.splice(i, 1);
                this.calculateTotals(cart);
            }
        }

    }

    /**
     * Clear cart of given session
     * @param {Object} request Request Object
     */
    static emptyCart(request) {
        if (request.session) {
            request.session.cart.items = [];
            request.session.cart.totals = 0.00;
        }
    }

    /**
     * Check if the product with the given productID exists in the user's shopping cart. 
     * Update the quantity if item exists
     * @param {ObjectId} productID Products ObjectID
     * @param {Object} cart User's shopping cart
     * @param {Int} quantity Item purchase quantity
     */
    static inCart(productID = null, cart, quantity = 1) {
        let found = false;
        cart.items.forEach(item => {
            if (item.id.toString() === productID.toString()) {
                item.quantity += quantity;
                found = true;
            }
        });
        return found;
    }

    /**
     * Calculate the total amount and update the cart
     * @param {Object} cart User's shopping cart
     */
    static calculateTotals(cart) {
        cart.totals = 0.00;
        cart.items.forEach(item => {
            let amount = item.price * item.quantity;
            cart.totals += amount;
        });
    }
}

module.exports = Cart;