(() => {

    'use strict';

    /**
	 * Prepare common response JSON
	 * @param {string} code - Error code
	 * @param {string} attribute - Attribute name. This should available only for validation errors
	 * @param {object} data - Any other date to be sent. Ex:view record data
	 * @param {string} message - Optional message to be sent
	 * @returns {object}
	 */
    var common = function (code, attribute = '', data = '', message = '') {
        var msg = {
            'code': code,
            'attribute': attribute,
            'data': data,
            'message': message
        }

        return msg;
    }

    module.exports = {
        MISSING_MANDATORY_ATTRIBUTE: 'MISSING_MANDATORY_ATTRIBUTE',
        INSUFFICIENT_LENGTH: 'INSUFFICIENT_LENGTH',
        EMAIL_IN_USE: 'EMAIL_IN_USE',
        INVALID_VALUE: 'INVALID_VALUE',
        RECORD_NOT_FOUND: 'RECORD_NOT_FOUND',
        common: common
    }
})();