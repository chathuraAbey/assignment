(() => {

    'use strict';

    const { param } = require('express-validator/check');
    let responseMessages = require('../lib/responseMessages');
    const User = require('../models/user');

    exports.get = function () {
        return [
            param('id').exists().withMessage(responseMessages.MISSING_MANDATORY_ATTRIBUTE),
            param('id').custom(checkIfRecordExists).withMessage(responseMessages.RECORD_NOT_FOUND)
        ];
    };

    exports.update = function () {
        return [
            param('id').exists().withMessage(responseMessages.MISSING_MANDATORY_ATTRIBUTE),
            param('id').custom(checkIfRecordExists).withMessage(responseMessages.RECORD_NOT_FOUND)
        ];
    };

    exports.delete = function () {
        return [
            param('id').exists().withMessage(responseMessages.MISSING_MANDATORY_ATTRIBUTE),
            param('id').custom(checkIfRecordExists).withMessage(responseMessages.RECORD_NOT_FOUND)
        ];
    };

    async function checkIfRecordExists(id) {
        let user = await User.findOne({ _id: id }).exec();
        if (user) {
            return true;
        } else {
            return false;
        }
    }

})();