(() => {

	'use strict';

	const { body } = require('express-validator/check');
	const responseMessages = require('../lib/responseMessages');
	const User = require('../models/user');

	exports.signup = function () {
		return [
			body('firstName').exists().withMessage(responseMessages.MISSING_MANDATORY_ATTRIBUTE).trim(),
			body('lastName').exists().withMessage(responseMessages.MISSING_MANDATORY_ATTRIBUTE).trim(),
			body('password').exists().withMessage(responseMessages.MISSING_MANDATORY_ATTRIBUTE).trim(),
			body('email').exists().withMessage(responseMessages.MISSING_MANDATORY_ATTRIBUTE).normalizeEmail(),
			body('email').isEmail().withMessage(responseMessages.INVALID_EMAIL),
			body('email').custom(checkIfEmailAvailable).withMessage(responseMessages.EMAIL_IN_USE),
			body('password').isLength({ min: 5 }).withMessage(responseMessages.INSUFFICIENT_LENGTH)
		];
	};

	exports.authenticate = function () {
		return [
			body('email').exists().withMessage(responseMessages.MISSING_MANDATORY_ATTRIBUTE).normalizeEmail(),
			body('email').isEmail().withMessage(responseMessages.INVALID_EMAIL),
			body('password').exists().withMessage(responseMessages.MISSING_MANDATORY_ATTRIBUTE)
		];
	};

	async function checkIfEmailAvailable(email) {
		let user = await User.findOne({ email: email }).exec();
		if (user) {
			return false;
		} else {
			return true;
		}
	}

})();