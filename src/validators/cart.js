(() => {

    'use strict';

    const { body, param } = require('express-validator/check');
    const responseMessages = require('../lib/responseMessages');
    const Product = require('../models/product');

    exports.removeFromCart = function () {
        return [
            param('productId').exists().withMessage(responseMessages.MISSING_MANDATORY_ATTRIBUTE).trim(),
            param('productId').custom(checkIfProductAvailable).withMessage(responseMessages.RECORD_NOT_FOUND)
        ];
    };

    exports.addToCart = function () {
        return [
            body('productId').exists().withMessage(responseMessages.MISSING_MANDATORY_ATTRIBUTE).trim(),
            body('productId').custom(checkIfProductAvailable).withMessage(responseMessages.RECORD_NOT_FOUND),
            body('quantity').exists().withMessage(responseMessages.MISSING_MANDATORY_ATTRIBUTE).isInt({ gt: 0 })
        ];
    };

    exports.updateCart = function () {
        return [
            body('productId').exists().withMessage(responseMessages.MISSING_MANDATORY_ATTRIBUTE).trim(),
            body('quantity').exists().withMessage(responseMessages.MISSING_MANDATORY_ATTRIBUTE)
        ];
    };

    async function checkIfProductAvailable(productId) {
        let product = await Product.findOne({ _id: productId }).exec();
        if (product) {
            return true;
        } else {
            return false;
        }
    }
})();