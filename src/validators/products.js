(() => {

	'use strict';

	const { body, param } = require('express-validator/check');
	const responseMessages = require('../lib/responseMessages');
	const Product = require('../models/product');

	exports.create = function () {
		return [
			body('name').exists().withMessage(responseMessages.MISSING_MANDATORY_ATTRIBUTE),
			body('price').exists().withMessage(responseMessages.MISSING_MANDATORY_ATTRIBUTE),
			body('quantity').optional().isInt({ gt: 0 })
		];
	};

	exports.get = function () {
		return [
			param('id').exists().withMessage(responseMessages.MISSING_MANDATORY_ATTRIBUTE).trim(),
			param('id').custom(checkIfProductAvailable).withMessage(responseMessages.RECORD_NOT_FOUND)
		];
	};

	exports.update = function () {
		return [
			param('id').exists().withMessage(responseMessages.MISSING_MANDATORY_ATTRIBUTE).trim(),
			param('id').custom(checkIfProductAvailable).withMessage(responseMessages.RECORD_NOT_FOUND)
		];
	};

	exports.delete = function () {
		return [
			param('id').exists().withMessage(responseMessages.MISSING_MANDATORY_ATTRIBUTE).trim(),
			param('id').custom(checkIfProductAvailable).withMessage(responseMessages.RECORD_NOT_FOUND)
		];
	};

	async function checkIfProductAvailable(productId) {
		let product = await Product.findOne({ _id: productId }).exec();
		if (product) {
			return true;
		} else {
			return false;
		}
	}
})();