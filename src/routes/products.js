const express = require('express');
const router = express.Router();
const controller = require('../controllers/product');
const validator = require('../validators/products');
const { handleValidationError } = require('../lib/helper');

router.post('/', validator.create(), handleValidationError, controller.create);
router.get('/:id', validator.get(), handleValidationError, controller.get);
router.get('/', controller.list);
router.delete('/:id', validator.delete(), handleValidationError, controller.delete);

module.exports = router;
