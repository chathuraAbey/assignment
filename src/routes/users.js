var express = require('express');
var router = express.Router();
var controller = require('../controllers/user');
const validator = require('../validators/users');
const { handleValidationError } = require('../lib/helper');

/* GET user */
router.get('/:id', validator.get(), handleValidationError, controller.get);

/* GET users listing. Order matters, we should always keep this function at the last of GET */
router.get('/', controller.list);

/* PATCH update user. PATCH is used as partial update is performed */
router.patch('/:id', validator.update(), handleValidationError, controller.update);

/* DELETE a user. */
router.delete('/:id', validator.delete(), handleValidationError, controller.delete);

module.exports = router;