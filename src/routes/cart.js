const express = require('express');
const router = express.Router();
const controller = require('../controllers/cart');
const validator = require('../validators/cart');
const { handleValidationError } = require('../lib/helper');

router.get('/', controller.initializeSession, controller.get);
router.post('/', validator.addToCart(), handleValidationError, controller.initializeSession, controller.addToCart);
router.patch('/', validator.updateCart(), handleValidationError, controller.initializeSession, controller.updateCart);
router.delete('/product/:productId', validator.removeFromCart(), handleValidationError, controller.initializeSession, controller.removeFromCart);
router.delete('/', controller.emptyCart);

module.exports = router;
