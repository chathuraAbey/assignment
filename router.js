const express = require('express');
const router = express.Router();

// Setup routers
const authenticateRouter = require('./authenticate');
const usersRouter = require('./src/routes/users');
const productRouter = require('./src/routes/products');
const cartRouter = require('./src/routes/cart');
router.use('/', authenticateRouter);
router.use('/users', usersRouter);
router.use('/products', productRouter);
router.use('/cart', cartRouter);

// catch 404 and forward to error handler
router.use(function (req, res, next) {
    res.status(404);
    res.send('Not found');
});

module.exports = router;