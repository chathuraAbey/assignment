var supertest = require('supertest');
var should = require('should');
var config = require('config');

var server = supertest.agent(config.get('automation.baseUrl'));
var responseMessages = require('../../src/lib/responseMessages');

var header = {
    'Content-type': 'application/json'
}

var accessToken = '';
var adminToken = '';
var userId = '';
var productId = '';
var productPrice = 300;

describe('Automation tests', () => {

    // Signup user
    it('Should sign up a user to the system', function (done) {

        this.timeout(10000);

        var data = {
            'firstName': 'Simon',
            'lastName': 'Mathew',
            'email': 'automation@aswat.com',
            'password': 'test.123'
        };

        server
            .post('/authenticate/signup')
            .set(header)
            .send(data)
            .expect('Content-type', /json/)
            .expect(201)
            .end(function (err, res) {
                res.body.firstName.should.equal('Simon');
                res.body.lastName.should.equal('Mathew');
                res.body.email.should.equal('automation@aswat.com');
                userId = res.body.id;
                done();
            });
    });

    // Login
    it('Should login the user', function (done) {

        this.timeout(10000);

        var data = {
            'email': 'automation@aswat.com',
            'password': 'test.123'
        };

        server
            .post('/authenticate/login')
            .set(header)
            .send(data)
            .expect('Content-type', /json/)
            .expect(201)
            .end(function (err, res) {
                res.body.success.should.equal(true);
                res.body.user.id.should.equal(userId);
                res.body.user.firstName.should.equal('Simon');
                res.body.user.lastName.should.equal('Mathew');
                res.body.user.email.should.equal('automation@aswat.com');
                accessToken = res.body.accessToken;
                done();
            });
    });

    // Login
    it('Should login the admin', function (done) {

        this.timeout(10000);

        var data = {
            'email': 'super@aswat.com',
            'password': 'secret'
        };

        server
            .post('/authenticate/login')
            .set(header)
            .send(data)
            .expect('Content-type', /json/)
            .expect(201)
            .end(function (err, res) {
                res.body.success.should.equal(true);
                res.body.user.firstName.should.equal('Super');
                res.body.user.lastName.should.equal('Administrator');
                res.body.user.email.should.equal('super@aswat.com');
                adminToken = res.body.accessToken;
                done();
            });
    });

    // Add product
    it('Should be able to add Product', function (done) {

        this.timeout(10000);

        let thisHeader = header;
        thisHeader.Authorization = 'Bearer ' + adminToken;

        var data = {
            'name': 'myproduct 1',
            'price': productPrice,
            'quantity': 5,
            'description': 'test description'
        };

        server
            .post('/products')
            .set(header)
            .send(data)
            .expect('Content-type', /json/)
            .expect(201)
            .end(function (err, res) {
                res.body.name.should.equal('myproduct 1');
                res.body.price.should.equal(300);
                res.body.quantity.should.equal(5);
                res.body.description.should.equal('test description');
                productId = res.body._id;
                done();
            });

    });

    // Add to cart
    it('Should add a product to Cart', function (done) {

        this.timeout(10000);

        let thisHeader = header;
        thisHeader.Authorization = 'Bearer ' + accessToken;

        var data = {
            'productId': productId,
            'quantity': 2
        };

        server
            .post('/cart')
            .set(header)
            .send(data)
            .expect('Content-type', /json/)
            .expect(201)
            .end(function (err, res) {
                res.body.items[0].id.should.equal(productId);
                res.body.items[0].price.should.equal(productPrice);
                res.body.items[0].quantity.should.equal(2);
                res.body.items[0].description.should.equal('test description');
                res.body.totals.should.equal(productPrice * 2);
                done();
            });

    });

    // Empty Cart
    it('Should empty cart', function (done) {

        this.timeout(10000);

        let thisHeader = header;
        thisHeader.Authorization = 'Bearer ' + accessToken;

        server
            .delete('/cart')
            .set(header)
            .send()
            .expect('Content-type', /json/)
            .expect(200)
            .end(() => {
                done();
            });

    });

    // Delete product
    it('Should be able to delete Product', function (done) {

        this.timeout(10000);

        let thisHeader = header;
        thisHeader.Authorization = 'Bearer ' + adminToken;

        server
            .delete('/products/' + productId)
            .set(header)
            .send()
            .expect('Content-type', /json/)
            .expect(200)
            .end(() => {
                done();
            });

    });

    // Delete user
    it('Should be able to delete a User', function (done) {

        this.timeout(10000);

        let thisHeader = header;
        thisHeader.Authorization = 'Bearer ' + adminToken;

        server
            .delete('/users/' + userId)
            .set(header)
            .send()
            .expect('Content-type', /json/)
            .expect(200)
            .end(() => {
                done();
            });

    });
});