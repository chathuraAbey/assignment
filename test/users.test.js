const expect = require('chai').expect;
const sinon = require('sinon');
const User = require('../src/models/user');
const controller = require('../src/controllers/user');
const httpMocks = require('node-mocks-http');
const basePath = '/users'

// Mocked User object
const userMock = {
    id: 1234567890,
    firstName: 'layansan',
    lastName: 'rajendram',
    email: 'test@unit.com',
    password: 'somethingrandom',
    toClient: function () {
        delete this.password;
        return this;
    },
    save: function (callback) {
        return callback(null);
    }
};
/**
 * Test user sign up success scenario
 */
it('Should sign up new user', function (done) {

    // Hacking the constructor
    User.prototype = new function () {
        let usr = new User({ userMock });
        usr.save = userMock.save;
        return usr;
    };

    // Create mock request
    let request = httpMocks.createRequest({
        method: 'POST',
        url: basePath,
        body: {
            firstName: userMock.firstName,
            lastName: userMock.lastName,
            email: userMock.email
        }
    });

    let response = httpMocks.createResponse();
    controller.create(request, response);

    let data = JSON.parse(response._getData());
    expect(data.success).to.be.equal(true);

    done();
});

/**
 * Test User get success scenario
 */
it('Should get user', function (done) {

    //Stub find method
    sinon.stub(User, 'findOne').yields(null, userMock);

    // Create mock request
    let request = httpMocks.createRequest({
        method: 'GET',
        url: basePath + '/' + userMock.id,
        params: {
            id: userMock.id
        }
    });

    let response = httpMocks.createResponse();
    controller.get(request, response);

    let data = JSON.parse(response._getData());
    expect(data.id).to.be.equal(userMock.id);
    expect(data.firstName).to.be.equal(userMock.firstName);
    expect(data.lastName).to.be.equal(userMock.lastName);
    expect(data.email).to.be.equal(userMock.email);

    done();
});