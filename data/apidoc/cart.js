/************ GET User's Cart *********/
 
/**
 * @api {get} http://<base-url>/cart Get Cart
 * @apiDescription Retrieve shopping cart of the logged in user who makes the request
 * 
 * @apiName GetCart
 * @apiGroup Cart
 *
 * @apiExample Example Request:
 *    {
 *      "firstName":"Layansan",
 *      "lastName":"Rajendram",
 *      "email":"mymail@gmail.com",
 *      "password":"TESTtest1"
 *    }
 * 
 * @apiSuccess {String} name Item name.
 * @apiSuccess {String} description Item description.
 * @apiSuccess {Number} Price Item price (of one unit).
 * @apiSuccess {id} id Product record ObjectID (UUID).
 * @apiSuccess {Number} totals Total cart value.
 * 
 * @apiSuccessExample Success-Response Sample:
 * {
 *  "items": [
 *       {
 *            "id": "5b8d17f050bc7309c0019d8c",
 *            "name": "myproduct 1",
 *            "description": "",
 *            "price": 100,
 *            "quantity": 1
 *        }
 *    ],
 *    "totals": 100
 * }
 */

/************ Add to Cart *********/
 
/**
 * @api {post} http://<base-url>/cart Add to Cart
 * @apiDescription Add a product to user's shopping cart. 
 * - Authorization token required in header.
 * @apiHeaderExample {json} Authorization header-Example:
 * {
 *      "Authorization": "Bearer XXXXXGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjViOGNlZDM0ZmM5NWI3MmFhYzJjZWZhOSIsInVzZXJUeXBlIjoxLCJpYXQiOjE1MzYwNDQzNTIsImV4cCI6MTUzNjEzMDc1Mn0.l3WHJjBWAROo-3nvlbwQQ82lUDsZVsrk-zbnKYekt0Y"
 * }
 *
 * @apiName AddToCart
 * @apiGroup Cart
 *
 * @apiParam {String} productId Product ObjectID (UUID).
 * @apiParam {Number} quantity Purchase quantity.
 *
* @apiExample Example Request:
 * {
 *      "productId": "5b8d17f050bc7309c0019d8c",
 *      "quantity": 1
 *  }
 *
 * @apiSuccessExample Success-Response Sample:
 * {
 *  "items": [
 *       {
 *            "id": "5b8d17f050bc7309c0019d8c",
 *            "name": "myproduct 1",
 *            "description": "",
 *            "price": 100,
 *            "quantity": 1
 *        }
 *    ],
 *    "totals": 100
 * }
 */

 /************ Remove from Cart *********/
 
/**
 * @api {post} http://<base-url>/cart/product/:productId Remove from Cart
 * @apiDescription Remove a product from user's shopping cart. 
 * - Authorization token required in header.
 * @apiHeaderExample {json} Authorization header-Example:
 * {
 *      "Authorization": "Bearer XXXXXGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjViOGNlZDM0ZmM5NWI3MmFhYzJjZWZhOSIsInVzZXJUeXBlIjoxLCJpYXQiOjE1MzYwNDQzNTIsImV4cCI6MTUzNjEzMDc1Mn0.l3WHJjBWAROo-3nvlbwQQ82lUDsZVsrk-zbnKYekt0Y"
 * }
 *
 * @apiName RemoveFromCart
 * @apiGroup Cart
 *
 * @apiParam {String} productId Product ObjectID (UUID).
 *
 * @apiSuccessExample Success-Response Sample:
 * {
 *  "items": [
 *       {
 *            "id": "5b8d17f050bc7309c0019d8c",
 *            "name": "myproduct 1",
 *            "description": "",
 *            "price": 100,
 *            "quantity": 1
 *        }
 *    ],
 *    "totals": 100
 * }
 */


/************ Update Cart *********/
 
/**
 * @api {patch} http://<base-url>/cart Update cart
 * @apiDescription Update user's shopping cart. Provide productId, quantity as single value or as an array to do multiple update
 * - Authorization token required in header.
 * @apiHeaderExample {json} Authorization header-Example:
 * {
 *      "Authorization": "Bearer XXXXXGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjViOGNlZDM0ZmM5NWI3MmFhYzJjZWZhOSIsInVzZXJUeXBlIjoxLCJpYXQiOjE1MzYwNDQzNTIsImV4cCI6MTUzNjEzMDc1Mn0.l3WHJjBWAROo-3nvlbwQQ82lUDsZVsrk-zbnKYekt0Y"
 * }
 *
 * @apiName AddToCart
 * @apiGroup Cart
 *
 * @apiParam {String} productId Product ObjectID (UUID) []Array for multiple.
 * @apiParam {Number} quantity Purchase quantity []Array for multiple..
 *
* @apiExample Example Request:
 * {
 *      "productId": ["5b8d17f050bc7309c0019d8c", "5b8d17f050bc7309c0019d8d"],
 *      "quantity": [1, 2]
 *  }
 *
 * @apiSuccessExample Success-Response Sample:
 * {
 *  "items": [
 *       {
 *            "id": "5b8d17f050bc7309c0019d8c",
 *            "name": "myproduct 1",
 *            "description": "",
 *            "price": 100,
 *            "quantity": 1
 *        }
 *    ],
 *    "totals": 100
 * }
 */

 /************ Empty Cart *********/
 
/**
 * @api {delete} http://<base-url>/cart Empty Cart
 * @apiDescription Empty user's shopping cart.
 * - Authorization token required in header.
 * @apiHeaderExample {json} Authorization header-Example:
 * {
 *      "Authorization": "Bearer XXXXXGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjViOGNlZDM0ZmM5NWI3MmFhYzJjZWZhOSIsInVzZXJUeXBlIjoxLCJpYXQiOjE1MzYwNDQzNTIsImV4cCI6MTUzNjEzMDc1Mn0.l3WHJjBWAROo-3nvlbwQQ82lUDsZVsrk-zbnKYekt0Y"
 * }
 *
 * @apiName EmptyCart
 * @apiGroup Cart
 *
 */