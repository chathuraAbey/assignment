/************ Get User *********/

/**
 * @api {get} http://<base-url>/users/:id Get User
 * @apiDescription Retrieve existing user details. 
 * - Authorization token required in header.
 * @apiHeaderExample {json} Authorization header-Example:
 * {
 *      "Authorization": "Bearer XXXXXGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjViOGNlZDM0ZmM5NWI3MmFhYzJjZWZhOSIsInVzZXJUeXBlIjoxLCJpYXQiOjE1MzYwNDQzNTIsImV4cCI6MTUzNjEzMDc1Mn0.l3WHJjBWAROo-3nvlbwQQ82lUDsZVsrk-zbnKYekt0Y"
 * }
 *
 * @apiName GetUser
 * @apiGroup User
 *
 * @apiParam {string} id User ObjectID (UUID).
 *
* @apiSuccessExample Success-Response Sample:
 * {
 *      "lastName": "rajendram",
 *      "userType": 2,
 *      "email": "mymail@gmail.com",
 *      "firstName": "layansan",
 *      "createdAt": "2018-09-06T05:55:51.208Z",
 *      "updatedAt": "2018-09-06T05:55:51.208Z",
 *      "id": "5b90c167c870c9079836b01c"
 *  }
 *
 * @apiExample Example Request:
 * /users/5b90c167c870c9079836b01c
 *
 */

/************ List users *********/

/**
 * @api {get} http://<base-url>/users List users
 * @apiDescription Retrieve list of Users. 
 * - Authorization token required in header.
 * @apiHeaderExample {json} Authorization header-Example:
 * {
 *      "Authorization": "Bearer XXXXXXciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjViOGNlZDM0ZmM5NWI3MmFhYzJjZWZhOSIsInVzZXJUeXBlIjoxLCJpYXQiOjE1MzYwNDQzNTIsImV4cCI6MTUzNjEzMDc1Mn0.l3WHJjBWAROo-3nvlbwQQ82lUDsZVsrk-zbnKYekt0Y"
 * }
 *
 * @apiName ListUsers
 * @apiGroup User
 *
 * @apiParam {number} [page] Page number.
 * @apiParam {number} [limit] Records per page.
 *
 * @apiSuccessExample Success-Response:
 * {
 *   "docs": [
 *       {
 *           "_id": "5b8ced34fc95b72aac2cefa9",
 *           "lastName": "rajendram",
 *           "userType": 2,
 *           "email": "layansann@example.com",
 *           "firstName": "layansan",
 *           "createdAt": "2018-09-03T08:13:40.698Z",
 *           "updatedAt": "2018-09-03T08:13:40.698Z",
 *           "id": "5b8ced34fc95b72aac2cefa9"
 *       },
 *       {
 *           "_id": "5b90c167c870c9079836b01c",
 *           "lastName": "rajendram",
 *           "userType": 2,
 *           "email": "layansan+n@example.test",
 *           "firstName": "layansan",
 *           "createdAt": "2018-09-06T05:55:51.208Z",
 *           "updatedAt": "2018-09-06T05:55:51.208Z",
 *           "id": "5b90c167c870c9079836b01c"
 *       }
 *   ],
 *   "total": 2,
 *   "limit": 3,
 *   "page": 1,
 *   "pages": 1
 * }
 *
 * @apiExample Example Request:
 * /users?page=1&limit=3
 *
 */

 /************ Update User *********/

/**
 * @api {patch} http://<base-url>/users/:id Update User
 * @apiDescription Update existing user details such as firstName, lastName. This endpoint can not be used to update login information such as email/password 
 * - Authorization token required in header.
 * @apiHeaderExample {json} Authorization header-Example:
 * {
 *      "Authorization": "Bearer XXXXXGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjViOGNlZDM0ZmM5NWI3MmFhYzJjZWZhOSIsInVzZXJUeXBlIjoxLCJpYXQiOjE1MzYwNDQzNTIsImV4cCI6MTUzNjEzMDc1Mn0.l3WHJjBWAROo-3nvlbwQQ82lUDsZVsrk-zbnKYekt0Y"
 * }
 *
 * @apiName GetUser
 * @apiGroup User
 *
 * @apiParam {string} id User ObjectID (UUID).
 *
 * @apiExample Example Request Body:
 *    {
 *      "firstName":"Layansan",
 *      "lastName":"Rajendram"
 *    }
 * @apiExample Example Request Path:
 * /users/5b90c167c870c9079836b01c
 *
 */
