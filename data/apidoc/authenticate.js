/************ Signup User *********/
 
/**
 * @api {post} http://<base-url>/authenticate/signup User Signup
 * @apiDescription Sign up a new user (customer) to the system.
 * 
 * @apiName UserSignup
 * @apiGroup User
 *
 * @apiExample Example Request:
 *    {
 *      "firstName":"Layansan",
 *      "lastName":"Rajendram",
 *      "email":"mymail@gmail.com",
 *      "password":"TESTtest1"
 *    }
 * 
 * @apiSuccess {String} firstname Firstname of the User.
 * @apiSuccess {String} lastname  Lastname of the User.
 * @apiSuccess {String} email Email of the User.
 * @apiSuccess {Number} userType Type of the User 1:Staff, 2:Customer.
 * @apiSuccess {String} createdAt User record created timestamp.
 * @apiSuccess {String} updatedAt User record updated timestamp.
 * @apiSuccess {String} id User record ObjectID (UUID).
 * 
 * @apiSuccessExample Success-Response Sample:
 * {
 *      "lastName": "rajendram",
 *      "userType": 2,
 *      "email": "mymail@gmail.com",
 *      "firstName": "layansan",
 *      "createdAt": "2018-09-06T05:55:51.208Z",
 *      "updatedAt": "2018-09-06T05:55:51.208Z",
 *      "id": "5b90c167c870c9079836b01c"
 *  }
 */


/************ Login User *********/

/**
 * @api {post} http://<base-url>/authenticate/login Login user
 * @apiDescription Login using username,password and obtain accessToken to be used for API calls.
 * 
 * @apiName LoginUser
 * @apiGroup Authenticate
 *
 * @apiExample Example Request:
 *    {
 *      "email":"mymail@gmail.com",
 *      "password":"TESTtest1"
 *    }
 * 
 * @apiSuccess {String} firstname Firstname of the User.
 * @apiSuccess {String} lastname  Lastname of the User.
 * @apiSuccess {String} email Email of the User.
 * @apiSuccess {boolean} success True if login is success; false otherwise.
 * @apiSuccess {String} message Login message dependends on login status.
 * @apiSuccess {String} accessToken Access token to be used by the user to authenticate API calls.
 * @apiSuccess {String} id User record ObjectID (UUID).
 * 
 * @apiSuccessExample Success-Response Sample:
 * {
 *   "success": true,
 *   "message": "Successfully authenticated.",
 *   "user": {
 *       "id": "5b90c167c870c9079836b01c",
 *       "firstName": "layansan",
 *       "lastName": "rajendram",
 *       "email": "mymail@gmail.com"
 *   },
 *   "accessToken": "XXXXXGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjViOGNlZDM0ZmM5NWI3MmFhYzJjZWZhOSIsInVzZXJUeXBlIjoyLCJpYXQiOjE1MzYyMTM4NTAsImV4cCI6MTUzNjMwMDI1MH0.VXlgNZJe9zjhwawgwfE2b2soMluKV93v4Hx4V1SulGE"
 * }
 */

 /************ Logout User *********/

/**
 * @api {delete} http://<base-url>/authenticate/logout Logout user
 * @apiDescription Logout user.
 * 
 * @apiName LogoutUser
 * @apiGroup Authenticate
 */