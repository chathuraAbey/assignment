
> ### ASWAT REST API Node (Express + Mongoose)

API Documentation located in public/apidoc folder

# Getting started

To get the Node server running locally:

- Clone this repo
- `npm install` to install all required dependencies
- Install MongoDB Community Edition ([instructions](https://docs.mongodb.com/manual/installation/#tutorials)) and run it by executing `mongod`
- `npm run dev` to start the local server

## Running db migration

Initial migration script has been added to create admin user with default credentials:

Email: super@aswat.com

Password: secret

Documentation on running migration: https://db-migrate.readthedocs.io/en/latest/Getting%20Started/commands/#commands

Example:
```sh
$ node .\node_modules\db-migrate\bin\db-migrate up --config .\config\db-migration.json
```


**Note**: This approach should be changed an option should be added to change the super admin password once created.

## Postman Collection

Sample postman request collection import file `aswat.postman_collection.json` available under data.

## Running test

Install Mocha Globally (Recommended)
```sh
$ sudo npm install -g mocha
```

Or locally (Already added as dev dependecy, `npm install` would have already downloaded in dev env)

# Run the integration test
```sh
$ mocha test/integration
```

# Run unit test

```sh
$ mocha
```
Or
```sh
$ npm test
```
# API Document
API Documentation available under public/apidoc folder

# Code Overview

## Dependencies

- [expressjs](https://github.com/expressjs/express) - The server for handling and routing HTTP requests
- [express-jwt](https://github.com/auth0/express-jwt) - Middleware for validating JWTs for authentication
- [jsonwebtoken](https://github.com/auth0/node-jsonwebtoken) - For generating JWTs used by authentication
- [mongoose](https://github.com/Automattic/mongoose) - For modeling and mapping MongoDB data to javascript 

## Application Structure

- `app.js` - The entry point to our application. This file defines our express server and connects it to MongoDB using mongoose. It also requires the routes and models we'll be using in the application.
- `config/` - This folder contains configuration/environment variables. Can have different files with the name of enviornment. default is added.
- `src/routes/` - This folder contains the route definitions for our API.
- `src/models/` - This folder contains the schema definitions for our Mongoose models.
- `src/validations/` - This folder contains request express-validators
- `data/apidoc/` - Data files used to generate API Doc.

## Authentication

Requests are authenticated using the `Authorization` header with a valid JWT. We define a express middlewares in `authenticate.js` that can be used to authenticate requests. The `required` middleware configures the `express-jwt` middleware using our application's secret and will return a 401 status code if the request cannot be authenticated.

**Note**: Only admin user can create Product.

#TODO:
- Add unit test, automation test
- ACL based on roles &amp; permissions
- Winston logger integration
- Change email/password
- Email verfication

Assignment by Layansan Rajendram.
