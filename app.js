const express = require('express');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const mongoose = require('mongoose');
const config = require('config');
const session = require('express-session');
const MongoDBStore = require('connect-mongodb-session')(session);
const app = express();
const router = require('./router');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

// Establish database connection
mongoose.connect(config.database, { useNewUrlParser: true });

// Initialize mongo session storage
const store = new MongoDBStore({
  uri: config.database,
  collection: config.get('session.collection')
});

// Initialize express session
app.use(session({
  secret: config.get('session.secret'),
  resave: false,
  saveUninitialized: true,
  unset: 'destroy',
  store: store,
  name: config.get('session.name'),
  maxAge: Date.now() + (config.get('session.maxAge'))
}));

// Setup router
app.use(router);

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // Send error response
  res.status(err.status || 500);
  res.send('Internal Server Error');
});

module.exports = app;
